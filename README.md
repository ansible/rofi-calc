# rofi-calc

Rather complicated in terms of dependencies, so it gets its own role.
Let's see if we can figure this out.

You need a C compilation toolchain:
+ `cc`
+ `autoconf`
+ `pkg-config`, ...

and `rofi` (version >= 1.5) as well as `qalculate` (version > 2.0).

According to the README, we will also need development headers for `rofi`.
But this is not an issue if we have installed rofi from source.

The C compilation chain is easy (my system is based on Ubuntu Server, so we already have all of it).

rofi >= v1.5 is also already fulfilled since we installed rofi from source
(see `playbooks/workstation/roles/i3wm/tasks/rofi.yml` for details).

`qalculate` is where this gets hairy.
`qalculate` consists of `qalc` (its CLI) and 
`qalculate-gtk` (its GUI).
Both of them depend on `libqalculate6`, which the Ubuntu repos offers:
```
taha@asks2:~
$ apt-cache madison libqalculate6
libqalculate6 |   0.9.10-1 | http://se.archive.ubuntu.com/ubuntu bionic/universe amd64 Packages
```

And herein lies the root of the problem.
According to the [rofi-calc docs](https://github.com/svenstaro/rofi-calc/wiki/Installing-libqalculate-from-source), 
version 0.9.10 of libqalculate is much too old to allow installing rofi-calc from source.

We will need to install a more recent version of libqalculate from source.

+ https://github.com/svenstaro/rofi-calc
+ https://github.com/svenstaro/rofi-calc/issues/11



## Install recent version of libqalculate

`libqalculate` offers

+ a [snap package](https://snapcraft.io/qalculate), `snap install qalculate`
+ a [flatpack package](https://flathub.org/apps/details/io.github.Qalculate), `flatpak install https://flathub.org/repo/appstream/io.github.Qalculate.flatpakref`
+ a [self-contained binary](https://github.com/Qalculate/qalculate-gtk/releases/download/v3.19.0/qalculate-3.19.0-x86_64.tar.xz)

The provided packages, particularly the self-contained binary, are a very nice
touch by the `libqalculate` maintainer and would probably fulfill our needs.
But since I'm writing this up as an Ansible role anyway, let's take the 
scenic route and install `libqalculate` from source.

+ https://github.com/Qalculate/libqalculate
+ https://qalculate.github.io/downloads.html
+ https://github.com/Qalculate/libqalculate/issues/192


### Install libqalculate from source

Requirements (also needs development packages of the same, if separate):

+ GMP and MPFR
+ libxml2
+ libcurl, icu, gettext (recommended)
+ iconv, readline (recommended for CLI)
+ Gnuplot (optional)
+ doxygen (for compilation of git version)

And here's how I identified the packages needed to fulfil each requirement above.

> [MPFR is based on the GMP multiple-precision library](https://www.mpfr.org/)

```
libgmp-dev - Multiprecision arithmetic library developers tools
libmpfr-dev - multiple precision floating-point computation developers tools
```

```
libxml2-dev - Development files for the GNOME XML library
```

I [identified this as the required curl library](https://github.com/Qalculate/libqalculate/issues/100#issuecomment-417557468):

```
libcurl4-openssl-dev
```

```
libicu-dev - Development files for International Components for Unicode
gettext - GNU Internationalization utilities
```

[`libiconv` is included in the `libc6` package, and its dev header files are found
in the `libc6-dev` package](https://askubuntu.com/questions/898826/why-is-there-no-libiconv-package-in-ubuntu).

```
libc6 - GNU C Library: Shared libraries
libc6-dev - GNU C Library: Development Libraries and Header Files
```

```
libreadline-dev - GNU readline and history libraries, development files
doxygen - Documentation system for C, C++, Java, Python and other languages
```



## After rofi-calc is installed 

### Integrate qalculate with i3 and rofi

Now that we have rofi-calc, integrating qalculate with rofi becomes very easy.

The [README has several examples](https://github.com/svenstaro/rofi-calc#advanced-usage). 
I decided to add the following to my i3 config file:

```
bindsym $ms+l exec --no-startup-id "rofi -show calc -modi calc -theme gruvbox-dark-soft -no-show-match -no-sort > /dev/null"
```


### More configuration

The file `~/.config/qalculate/qalc.cfg` is created automatically during installation.
It contains a wealth of configuration options. 
At some future date, we might want to integrate some of them into this role.



## Refs

+ https://reddit.com/r/linux/comments/ey4ips/lpt_units_is_a_unit_conversion_and_calculation/
+ https://qalculate.github.io/manual/
